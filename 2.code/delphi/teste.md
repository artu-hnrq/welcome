## Criação de testes unitários
Com base na calculadora, serão criados testes unitários para validar as functions criadas. Para isso, faça o merge do branch test_existing_project no seu branch :  

```bash
git merge origin/test_existing_project
```

Entre no diretório existing_project e abra o arquivo `calc.groupproj`.  
Tendo como base o arquivo sumTestU.pas, adicione arquivos DUnitX (Ler próxima seção) para as outras operações no existing_project_tests, importante que cada operação deve ter seu teste específico (arquivo '.pas').

[TestFixture]: indica que a classe a seguir é uma classe para testes e será executada como tal no debug;

[Setup]: indica que a procedure abaixo será executada uma vez apenas, logo no inicio do teste. Ideal para inicializações necessárias;

[TearDown]: indica que a procedure abaixo será executada apenas uma vez, ao fim do teste. Ideal para limpar locações de memória das variáveis inicializadas;

[Test]: indica que a procedure abaixo é um teste;

[TestCase]: utilizada para testar a mesma procedure mais de uma vez com testes diferentes;

Ao fim do desenvolvimento, faça o build e o debug dos testes.

## DUnitX
Framework para testes unitários, segue lista de leituras recomendadas:

[DUnitX Overview - Embarcadero](http://docwiki.embarcadero.com/RADStudio/Rio/en/DUnitX_Overview) (Documentação oficial (english) - contém overview geral, tutoriais e trechos de código)
[Testes Unitários com DUnitX - DEVMEDIA](https://www.devmedia.com.br/testes-unitarios-com-dunitx/36738) (Artigo em pt-br)
