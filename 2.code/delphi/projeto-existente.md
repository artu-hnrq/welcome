## Edição de um projeto existente

O projeto existente é simples, sendo uma calculadora. Para incorporá-lo ao seu branch, faça merge do branch existing_project no seu branch :  
```bash
git merge origin/existing_project
```
Entre no diretório existing_project e abra o arquivo `existing_project.dproj`.

Adicione os arquivos contendo a implementação ao invés de disparar uma exceção sobre a não implementação.
 - mulU.pas
 - divU.pas

O mulU deve conter uma função multiply que faz uma multiplicação e o divU uma função division que faz uma divisão(inteira - dica: **div**).

Adicione as opções no tratamento

Commite e publique suas alterações (`git add`, `git commit`, `git push`)

Após commitar, limpe sua árvore

```
git clean -fdx git reset --hard
```

Para validar que está tudo certo, compile o projeto no delphi  novamente e execute o script através da linha de comando (cmd ou powershell):

```
powershell -F scripts/validate_existing_project.ps1
```
