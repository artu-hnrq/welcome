### Projetos e arquivos

Projetos em Delphi possuem os seguintes arquivos:

- `pas` - Arquivo fonte.
- `dpr` - Arquivo fonte contendo o main de um determinado projeto.
- `dproj` - Configurações de um projeto específico em formato XML.
- `groupproj` - Grupo de dprojs, equivalente a uma solução do Visual Studio.

Importante: Projetos (dprojs) podem compartilhar os mesmos fontes em um mesmo repositório.

## Padrão de codificação
[Padrões Nelogica](https://drive.google.com/open?id=1vywkIx95MrIg_sI6h0EnqwqjrP7BE2oA) (importante, ler tudo)

## Codificação para sistemas críticos
[Boas praticas](https://drive.google.com/open?id=1b6bldpv6A97YqENa_ikLaC1PEk5-suLg) (importante, ler tudo)
