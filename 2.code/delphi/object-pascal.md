## Pascal Moderno
Leia o seguinte manual  
https://drive.google.com/open?id=1c5Ul5SiJLulFeYzk8Tid-2NXex6391SZ (importante, ler tudo)

Mais detalhes podem ser encontrados aqui:

http://docwiki.embarcadero.com/RADStudio/Tokyo/en/Delphi_Language_Reference (leitura informativa, aprenda onde buscar essas informações)

Leitura complementar:

- O'Reilly Delphi In a Nutshell (disponível no FTP na pasta Library)
