## Realizando consultas ao BD

Este documento demonstra uma forma de fazer consultas ao BD da Nelogica.

#### Instalação

1. Baixar e instalar o [Heidi SQL](https://www.heidisql.com/download.php);
2. Baixar e instalar o [driver OLEDB](https://docs.microsoft.com/pt-br/sql/connect/oledb/oledb-driver-for-sql-server);

#### Primeiro Uso

1. Abrir o Heidi SQL

2. Neste passo será necessário criar uma nova sessão clicando no botão **New**, destacado em amarelo no canto inferior esquerdo.
    ![New/Lista](img1.png)

3. Na opção **Network type**, destacada em (A), será selecionado **Microsoft SQL Server (TCP/IP)**.
    ![Network type](opt-1.png)

4. Na opção **Library**, destacada em (B), seleciona-se a biblioteca **MSOLEDBSQL**.
    ![Library](opt-2.png)

5. Na opção **Hostname / IP**, destacada em (C), é colocado o IP do servidor de BD.
    ![Hostname](opt-3.png)

6. Na opção **Port**, destacada em (D), é colocada a porta.

7. Na opção **User**, destacada em (E), é colocado o usuario para acesso ao BD.
    ![User](opt-4.png)

8. Na opção **Password**, destacada em (F), é colocada a senha para acesso ao BD.

9. Para abrir a conexão com o servidor é só clicar no botão **Open** no canto inferior direito, destacado amarelo.
    ![Open](open.png)

10. Uma vez aberta a conexão você tem acesso às bases contidas neste servidor.
    ![Conectado](main-view.png)
11. Em (G) temos a árvore com todas as bases do servidor. Em (H) temos a view principal onde são exibidas a maioria das informações.


Após a primeira conexão não será necessário realizar os passos anteriores uma vez que a configurações tenham sido salvas. Apenas será necessário modificar caso ocorram mudanças em um dos parâmetros (p. ex. senha ou IP).

#### Primeira consulta
Partindo da view principal, iremos fazer uma consulta ao banco **Mercado**. A consulta a ser feita está configurada para chamada como **Ativo_ListaCotacoes**.
1. Em (A) é possível verificar como está implementada a função.
    ![Overview Consulta](query-1.png)
2. Para executarmos a função clicaremos em **Query** (B).
3. O campo textual (C) é onde escrevemos a query/comando
    ![Consulta](query-2.png).
4. O botão (D) executa o comando escrito.
5. Podemos ver o resultado em (E).
    ![Resultado](query-3.png)


Autor: Guilherme Gaiardo	Data: 21/07/2020