### CLI
> <img src="/ico/blobwizard.png" width="30"/>
Acrónimo para *Command line interface*, que em português significa [interface de linha de comando][cli]

Uma CLI é um meio de interagir com um programa de computador, na qual se executa comandos através de um terminal rodando um [Shell][shell]. \
De modo geral, CLIs podem trazer uma série de subcommandos e *flags* para a configuração de sua execução.

Uma *flag* é um tipo especial de argumento usualmente precedido de dois hiféns (`-`) e que muitas vezes possuem uma alternativa mais curta com apenas um hifém. \
`--help` e `-h` são *flags* comumente disponíveis em CLIs e servem para auxiliar o uso do commando escolhido, apresentando informações sobre sua utilização.

[cli]: https://pt.wikipedia.org/wiki/Interface_de_linha_de_comandos
[shell]: /4.extra/shell.md
[git]: /1.intro/git.md
