# Instalar WatchDogInstrumentation.

Para fazer o download do watchdog é necessário acessar o servidor FTP da Nelogica, para isso é necessário utilizar um client FTP.

O mais utilizado pela equipe é o Filezilla, caso o filezilla não esteja instalado em sua maquina você pode baixa-lo em https://filezilla-project.org/download.php?type=client.

No servidor FTP acesse a pasta /Release/WatchDogInstrumentation/.

Procure a versão mais recente e faça o download do executável.

Feito isto execute o WatchDogInstrumentation.exe, ele solicitará algumas modificações no registo aceite todas.

# Instalar Controller

TODO

# Watchdog Comparer

O Watchdog Comparer é um serviço dentro da intrumentação que é responsável por listar todos os servidores de front do marketdata para o acompanhamento diário.

Nele é possível acompanhar atrasos em serviços, quantos servidores estão conectados, número de clientes ativos e etc.

Constantemente temos que atualizar a lista de servidores que o Watchdog Comparer está acompanhando, para isso devemos seguir os seguintes passos:

1. Identificar se existe algum servidor está desconectado na instrumentação, e não está aparecendo no controller. Podemos verificar qual é esse servidor clicando duas vezes no indicador da coluna. Alguns conjuntos de datacenter podem estar divididos em 2 colunas para não sobrecarregar o comparer. Isso indica que alguns endereços estão listados em servidores diferentes, com comparers diferentes rodando.
    ![AWSComparer](/img/awscomparer.jpg  "AWSComparer")

2. No servidor indicado procurar pelo arquivo WatchDogServer<DATACENTER>/config/ServerAddr.dat. Neste arquivo estão listados todos os servidores que aquele comparer está monitorando.
	![Path](/img/serverpath.jpg  "Path")

3. Dentro do arquivo deve-se adicionar ou remover endereços com o seguinte padrão nomedoserver.nelogica.com.br:ippublico:portadoserviço
	![File](/img/file.jpg  "File")

4. Após isso devem ser reiniciados os serviços do Comparer que foi modificado e do MainComparer.
