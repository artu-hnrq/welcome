# Electronic Trading Systems - International

A equipe de ETS Internacional é responsável pela integração de novas corretoras e features relacionadas ao ambiente internacional incluindo Criptomoedas, Foreign Exchange e ações internacionais.

O principal serviço utilizado é o [Hades](https://gitlab.nelogica.com.br/Roteamento/hades) e o passo a passo para sua instalação pode ser encontrado em seu repositório.

## Acesso aos servidores

Para ter acesso aos servidores você precisa solicitar ao time de Infraestrutura criando uma tarefa no Jira. As tarefas [DAT-4384](https://jira.nelogica.com.br/browse/DAT-4384) e [DAT-2598](https://jira.nelogica.com.br/browse/DAT-2598) foram criadas anteriormente por membros do time para isso, então você pode usá-las como modelo.

Os servidores de utilizados nos principais serviços do time ficam hospedados nos provedores Equinix e Amazon, então é para esses que Voce precisa solicitar acesso.

Para acesso aos servidores utilizamos o protocolo RDP e as instruções como login e endereço dos servidores podem ser encontradas [nesse link](http://infraredes.gitlab.nelogica.com.br/ansible/servermap_d89766621b08430d99afb2fff9f30d46.html)
