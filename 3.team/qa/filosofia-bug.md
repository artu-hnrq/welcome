# A filosofia do Bug

O que é um defeito (bug)? Um defeito é um desvio do comportamento esperado do software, ele pode ser evidente (percebido pelo cliente) ou latente (disparado por condições especiais ou outro bug, mas sem sintomas).

Além de defeito, temos outras classificações como falhas e erros. Falha é o bug latente, mas que não foi disparado, enquanto que erro é um problema mascarado (propositalmente ou não).

O gráfico a seguir ilustra quando o defeito aparece no processo de desenvolvimento, quando eles são encontrados e quanto custa o seu reparo. Note que quanto antes ele é detectado mais barato é. Esse custo não reflete somente o custo de correção, mas também as pessoas envolvidas, atrasos no processo, novos testes, impacto negativo e burocracia de registro.

![Curva de Bugs](images/bug_curve.png)

Gráfico extraído de <https://www.stickyminds.com/article/shift-left-approach-software-testing>

Uma forma conhecida para atacar esse problema é a pirâmide de testes, onde o número de verificações próxima ao código fonte é maior e esse número reduz (em número, não em complexidade) a medida que as unidades e sub-sistemas são integrados. Importante ressaltar que apesar de ser ideal, um (1) teste funcional acaba testando muito mais do sistema em um primeiro momento, pois uma execução já verifica boa parte das rotinas.

![Pirâmade de testes](images/test_pyramid.png)
