# Testes

Testes, especialmente automatizados, podem ser classificados em algumas dimensões:

- Escopo:  nível do sistema a ser testado, relacionado com o esforço e a precisão do teste
  - Unitário: uma unidade do sistema (geralmente uma classe ou um módulo);
  - Funcional: testa uma funcionalidade do sistema (não necessariamente exercitando todo o sistema);
  - Sistêmico: Testa o sistema como um todo (usando banco de dados, interfaces, etc);
- Transparência: quanto do sistema é conhecido pelo testador
  - Whitebox: testador conhece o interior e implementação do sistema;
  - Blackbox: testador só enxerga a interface do sistema;
  - "Graybox": parte extra do sistema é exposta para facilitar os testes;
- Objetivo: Para o que estou fazendo o teste
  - Validação: atende os requisitos?
  - Regressão: quebrei algo que já funcionava?
  - Aceitação: posso aceitar esse componente/serviço/produto para a próxima etapa?
  - Integração: esses 2 sub-sistemas que alterei/criei funcionam juntos?

## Questões

- O que é um mock?
- Como defino o escopo do teste?
- Qual o impacto de um falso positivo e um falso negativo em testes?
- Como tratar falsos positivos em testes?


### Diferença de validação e verificação

​	**Validação: foi implementado o que o cliente quer ?**

​	**Verificação: foi implementado o que o foi especificado ?**

## Questões

- O que é um bug?
- A verificação de software pode ser automatiza? E a validação de software?
- O que é necessário para automatizar a verificação de software?
