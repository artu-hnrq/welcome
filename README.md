<div align="center">
    <img src="/ico/nelogica.png">
</div>

# <img src="/ico/blob-wave.gif" width="50"/> **Saudações Nelógico**
#### Que bom te ver por aqui, seja muito bem vindo!

Nós preparamos esse projeto pra te acompanhar no primeiro contato com os fluxos e ferramentas que compõe o *processo de desenvolvimento de software* aqui da Nelogica! Vamos passar pelas tecnologias que usamos diariamente e também pelos conceitos de negócio, alinhando nosso conhecimento e te ajudando a deixar tudo pronto pra sua jornada conosco. Tá preparado?
<img src="/ico/meow_aww.png" width="30"/>

#### É o seguinte

Primeiro vale dizer que, pra ficar tudo organizado, separamos as informações por assuntos e você pode navegar por todos eles pelo menu lateral. Consulte a sua equipe pra saber quais deles são pertinentes para você nesse primeiro momento.

E pra ficar mais divertido, em cada módulo deixaremos algumas perguntas para que você possa conferir se entendeu tudo direitinho. Continue lendo para saber como enviar as suas respostas. E lembre que você não tá sozinho: Caso tenha dificuldade, não exite em pedir ajuda! Seu facilitador está aí pra isso. E se não consiguir com ele, pergunte a outros da sua equipe ou até mesmo no canal *geral* do [Teams][teams]. <br>
Aqui a gente joga junto!
  <img src="/ico/blob_fistbumpl.png" width="30"/>
  <img src="/ico/blob_fistbumpr.png" width="30"/>

[teams]: https://teams.microsoft.com/

---

> **P.S.:** Como tudo por aqui, esse roteiro está em melhoria contínua. Portanto, se você detectar algum erro ortográfico, alguma informação desatualizada, um link quebrado, ou mesmo sentir que uma explicação pode ser evoluida, fique a vontade para atualizar esse projeto!

---

#### Como é que vai ser?

Antes de mais nada, certifique-se que sua [VPN está configurada][vpn], que você tem acesso ao [GitLab][gitlab] e que já incluiu sua [chave SSH][ssh] por lá.

Para completar esse desafio você vai passar por todo o **fluxo de integração de código** que praticará durante suas atividades como desenvolvedor, entregando suas respostas no formato de uma proposta de evolução do código desse projeto.

As questões que você deve responder estarão dentro da pasta **answers** separadas em um arquivo para cada módulo. Siga o [Workflow][workflow] adicionando suas resposas de acordo conforme ler os conteúdo do módulo equivalente.

Conforme você for progredindo vamos explicando cada detalhe, siga confiante e boa leitura!
<img src="/ico/blob_thumbs_up.png" width="30"/>

[vpn]: /4.extra/vpn.md
[gitlab]: https://gitlab.nelogica.com.br
[ssh]: /4.extra/ssh.md
[workflow]: /1.intro/workflow.md
