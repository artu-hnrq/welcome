# Domínio de aplicação

O domínio das aplicações desenvolvidas pela Nelogica estão presentes no mercado financeiro. Essa etapa envolve a contextualização para esse escopo.

#### Conhecimento interno

Começando com alguns conhecimentos básicos divulgados pela Nelogica publicamente

- https://www.nelogica.com.br/conhecimento (leitura informativa, aprenda onde buscar essas informações)

#### Cursos da bolsa

Além disso, a própria bolsa de valores oferece cursos gratuitos:

- https://educacional.bmfbovespa.com.br/cursosonline (leitura informativa, aprenda onde buscar essas informações)

#### Documentação da bolsa

Do ponto de vista de aplicação, os protocolos de entrada de ofertas são interessantes.

- http://www.b3.com.br/pt_br/solucoes/plataformas/puma-trading-system/para-desenvolvedores-e-vendors/entrypoint-entrada-de-ofertas/ (leitura informativa, aprenda onde buscar essas informações)

Os protocolos de difusão de informações também são interessantes.

- http://www.b3.com.br/pt_br/solucoes/plataformas/puma-trading-system/para-desenvolvedores-e-vendors/umdf-sinal-de-difusao/ (leitura informativa, aprenda onde buscar essas informações)

## Questões
Da mesma forma realizada com o setup.md, crie na pasta `answers` um arquivo chamado conhecimento.md e responda as perguntas abaixo:

- Qual a diferença de trader e broker?
- Qual a diferença entre Análise Técnica e Análise Fundamentalista?
- O que é e para que serve um Gráfico de Candlestick?
- O que é uma opção no mercado financeiro?
- Qual a diferença entre Bovespa e BMF?
- Qual a diferença entre a opção americana e opção europeia com relação ao exercício de opções?
- Com relação a opções, o que é o valor temporal?
- O que é e para que serve o Simulador Nelogica?
- Qual a diferença entre FIX e FAST?
- Qual o significado das letras e números em PETR4 e WING19? E qual a diferença entre WINFUT e WING19?
