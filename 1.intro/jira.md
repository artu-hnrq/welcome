# *Jira* Software
O *Jira* é a plataforma de controle de tarefas e projetos utilizada na Nelogica. Ele permite compor tarefas em diferentes projetos, versões, sprints, etc. Além disso ele é capaz de centralizar informações de outras fontes (e.g. Gitlab).

Se quiser ver o *Jira* em ação, assista esse vídeo: https://www.youtube.com/watch?v=PQa3NFB_LRg&list=PLaD4FvsFdarR69HESUlY4IC7ae5k5znYp.

## Entidades
O *Jira* possui algumas entidades básicas que orquestradas possibilitam o seu funcionamento.

### Issues
Issue pode ser uma tarefa, um ticket ou um chamado. É a menor unidade de construção do *Jira*, cada Issue representa uma tarefa a ser realizada, possuindo um responsável, um estado, um tipo. Ela pertence a um projeto, que, associado ao tipo, define qual o fluxo de trabalho (workflow) essa issue deve seguir. Vários outros campos podem ser adicionados às *issues* (pelos administradores do *Jira*).  
Toda issue possui um identificador único, sendo, o prefixo, a chave do projeto e o sufixo um número dentro desse projeto.  
As *issues* possuem tipos padrão e também permitem a criação de tipos customizados. Alguns tipos são:

- story: uma entrega vista pelo cliente, descreve como o cliente (interno ou externo) irá enxergar a funcionalidade.
- epic: uma coleção  de stories (várias funcionalidades, melhorias ou correções) que possuem relação, por exemplo: melhorar a interface, melhorar a performance, adicionar uma forma do usuário visualizar a performance
- bugfix: correção de um bug existente no código
- feature: uma nova funcionalidade no software, não sendo necessariamente vista pelo cliente
- task: tarefa genérica (como validar a configuração de um servidor, criar um tutorial ou implementar uma prova de conceito)  
Além disso, *issues* também possuem prioridades, que definem qual a urgência e importância de cada issue. Uma pergunta para se fazer ao abrir uma issue é: algo pode interromper essa tarefa?

### Projects
Um projeto é um conjunto de *issues* relacionadas. Elas estão sob a supervisão de um mesmo time, este possuindo diversos papéis (administrador, project lead, desenvolvedor, stackholder, etc).  
Um projeto pode possuir "n" versões, que são um sub-conjunto de *issues* a serem liberadas em uma data específica. Além disso, *issues* podem ser associadas a componentes (1:N), esses componentes pertencem a um projeto e estão restritos ao escopo do projeto.  
Projetos podem assumir diversos tipos (business, software, service desk) e dentro destes tipos teremos "visualizações", associadas a metologias de desenvolvimento como kanban e Scrum. Essas visualizações permitem a criação de:

#### Workflows
Workflow é a definição dos estados e transições que a issue passa até ser completada. Apesar de não ser normalmente alterado pelo usuário, este é usado implicitamente em cada transição realizada. As transições podem ter ações associadas (como validar algo ou disparar alguma ação).

#### Boards/Quadros
Quadros organizam *issues* a partir de filtros, geralmente um único projeto. Elas mostram os estados em colunas e a *issues* como cartões. Arrastar as *issues* entre as colunas fazem as *issues* disparar uma transição.

Em algumas configurações, existe uma visão de quadro e outra de backlog. Esse artigo fala sobre isso: https://confluence.atlassian.com/jirasoftwareserver/using-your-kanban-backlog-938845378.html.

#### Filtros
Filtros são queries do *Jira*, eles podem ser salvos e utilizados para limitar quais *issues* aparecem em widgets.

#### Dashboards/Painéis
*Dashboards* são compostos por Widgets. Cada widget pode mostrar a informação em formatos diversos como listas, sumários, gráficos e pode ser filtrada por filtros implícitos ao widget ou salvos.

#### Prioridades
Prioridade é um campo de cada issue nativo do issue, vários filtros nativos e implementados se baseiam nela. As prioridades podem ser customizadas, porém na Nelogica usamos os valores padrões (default):
- Highest
- High
- Medium
- Low
- Lowest

Para definir uma prioridade, utilize o quadro a seguir como guia. Se não souber definir, consulte alguém mais experiente.

![Prioridades](/img/Prioridades.png?raw=true  "Prioridades")

Algumas perguntas para definir a prioridade:
- Quanto isso é urgente?
- Algo pode interromper essa tarefa?
- Qual o impacto financeiro dessa tarefa ser postergada?
- É pertinente que a liderança e a diretoria acompanhem essa tarefa especificamente?

## Boas práticas
Algumas dicas para usar o *Jira* com eficiência:
- Crie ou copie um dashboard para mostrar suas *issues* (abertas, em andamento, watched, em revisão, etc).
- Preencha novas *issues* de uma forma que quem for executa-la possa fazê-lo sem a necessidade de questionar ninguém a respeito.
- Avalie a prioridade cuidadosamente.
- Verifique periodicamente as tarefas em aberto do seu projeto.
- Use o *Jira* para registrar tarefas que podem ser feitas imediatamente, mas que você já mapeou a necessidade.
- Não use o *Jira* para "mostrar trabalho". Ninguém é promovido pelo número de tarefas sem levar em conta a qualidade delas.
- Use o *Jira*  para se comunicar com outras equipes usando relações entre *issues*, *issues* abertas em outras equipes, épicos entre equipes, etc.

## Primeiros Passos

Certifique-se que você tem um usuário no *Jira* junto com a equipe de infraestrutura de escritório. Após, logue-se no *Jira* acessando:  
https://*Jira*.nelogica.com.br

Verifique a quais projetos você está associado antes de prosseguir. Para se ambientar, use os seguintes passos:
- Visualize os *dashboards* existentes. Adicione o dashboard da sua equipe como favorito.
- Entre no seu projeto e visualize as *issues*, componentes e quadros.

Encontre como buscar por *issues*. Monte as seguintes queries:
- Todas as *issues*  assignadas e fechadas de um colega específico;
- Todas as *issues* criadas por outro colega e ainda abertas;
- Todas as *issues* abertas para um componente específico;

## Uso Rotineiro

Rotina definida pelo método *Scrum*:
https://www.atlassian.com/br/agile/scrum

A rotina do Jira deve ser sincronizada com a rotina do GitLab. Leia a seção de [Gitlab](#gitlab) primeiro.

CASO 1: A issue é sua e você está desenvolvendo uma solução para ela.
1. Selecionar suas *issues*: No menu esquerdo, clique em 'Backlog', clique na *issue* escolhida e, sob o menu "People" da *issue*, clique em "Assign to me" para se tornar o *Assignee*.
2. Trazer suas *issues* para o *sprint* atual: Na lista do 'Backlog', clique com o botão direito sobre a issue e a envie para a *sprint* corrente.
3. Na *sprint* ativa, arraste uma *issue* da coluna "TO DO" para a coluna *IN PROGRESS*, sinalizando que ela será trabalhada. Neste momento, faça alterações conforme as diretrizes descritas na seção de GitLab.
4. Quando ela for terminada, deve ser passada para a próxima coluna, "REVIEW". Abrirá uma janela, onde um revisor deve ser selecionado e uma breve descrição das mudanças deve ser feita.
5. Atente-se a eventuais sinalizações dos revisores. Caso algo esteja errado, você deve corrigir e alertar o revisor da correção. A *issue* não volta para a colune "IN PROGRESS". Caso não existam erros, você terminou seu trabalho.

CASO 2: Fui indicado como revisor para uma *issue*.
1. Usando a descrição e os comentários da *issue*, entenda o tópico e evolução do trabalho.
2. No merge request do GitLab, verifique falhas no pipeline. Sinalize o *Assignee* por mensagem.
3. Na aba "Changes" do merge request, identifique possíveis problemas no código ou pontos que podem ser melhorados. Sinalize o *Assignee* comentando os nas respectivas linhas. (Ao passar o mouse sobre o código, aparece um ícone de comentário na esquerda que deve ser clicado.)
4. Se mantenha em comunicação com o *Assignee* até a resolução dos problemas.
5. Quando nenhum topico de discussão estiver presente, passe a *issue* para a próxima etapa:
- Caso você seja o primeiro revisor, arraste a *issue* para a próxima coluna de revisão. informando o próximo revisor. Lembre-se de reatribuir o revisor do merge request do GitLab para o novo revisor.
- Caso você seja o último revisor, conclua a *issue* arrastando-a para a coluna "DONE". Não esqueça de aprovar o merge request.

## Questões

- Qual a relação entre projeto, issue e workflow?
- Como se define a prioridade de uma nova *issue*
- O que é JQL? (sim, não está aqui, google it)
- Qual a JQL para procurar por tarefas ainda abertas a mais de 6 meses em um projeto específico?
- Com qual Widget posso mostrar a JQL acima em um dashboard?
- Qual a diferença de um Watcher e um Assignee em uma *issue*?

Responda as questões no mesmo formato das seções anteriores e crie na pasta `answers` um arquivo chamado jira.md contendo as respostas.

https://gitlab.nelogica.com.br/Roteamento/hades/wikis/Gloss%C3%A1rio)
