# Informações Adicionais

## Microsoft Teams

Canal de comunicação da Nelogica. Principais abas:

- Atividade : Mostra as ultimas notificações recebidas
- Chat : Chat pessoal, permite conversar com outras pessoas da Nelogica
- Equipes: Grupos para comunicação interna de setores. **Importante: Ler as mensagens, quando receber, da equipe Geral**.

## Hades

Roteamento de Ordens

Informações adicionais, páginas extras na [wiki](https://gitlab.nelogica.com.br/Roteamento/hades/wikis/Gloss%C3%A1rio)

## Instalação de Ferramentas Adicionais - Ninite

Ninite oferece uma forma muito simples de eliminar a tarefa de instalação dos programas. Basta selecionar os programas que deseja instalar, e ele irá baixar e instalar automaticamente os programas selecionados.

Mais informações podem ser lidas em;
[https://ninite.com/](https://ninite.com/)

## Realizando Consultas ao BD

[Tutorial de consultas ao BD](consultas.md)

# Welcome por Equipe de Atuação

Parabéns pela conclusão deste roteiro.
Agora siga para o roteiro específico da **SUA ÁREA** de atuação.

[Quality Assurance](qa.md)

[Market Data](marketdata.md)

[Sistemas Internos](https://gitlab.nelogica.com.br/Sistemas_Internos/si_documentation/)

[OMS](oms.md)

[Electronic Trading Systems - Internacional](ets-internacional.md)
