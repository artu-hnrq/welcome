<div align="center">
    <img src="/ico/git.svg">
</div>

# Git

O git é um [sistema de controle de versões][vcs] *open-source*, um dos mais utilizados entre os desenvolvedores mundialmente. Os **VCS**, do inglês *version control system*, são uma categoria de ferramentas de software que contribuem no gerenciamento de modificações do código-fonte de uma aplicação ao longo do tempo.

Desenvolver software é, em boa parte do tempo, escrever código-fonte. E quando essa tarefa é realizada em equipe, este código sofre alterações simultâneas por diferentes desenvolvedores. Gerenciar as multiplas versões desse código e coordenar sua integração ao projeto é uma atividade importantíssima. E é para isso que usamos o git!

[vcs]: https://pt.wikipedia.org/wiki/Sistema_de_controle_de_vers%C3%B5es

## Instalação

Para instalar a versão mais atual do git para Windows, baixe o instalador [clicando aqui][git-for-windows]. Siga a instalação aceitando as definições predefinidas até que essa esteja concluída.

Para se certificar que o git está corretamente confirgurado, abra seu [powershell][shell] e execute o seguinte commando:
```
PS C:/Windows/> git version
git version 2.25.1
```

[git-for-windows]: https://git-scm.com/download/win
[shell]: /4.extra/shell.md


## Introdução
Dentre as estruturas usadas pelo git para gerir o versionamento, três são importantes compreendermos antes de prosseguir. A primeira delas é a **história**, que representa um conjunto sequencial de versões daquele repositório. Cada uma dessas versões é identificada por um **commit**, um ponto único da linha do tempo do projeto. E por fim temos os **branches**, ramificações dessa história que se interligam em um determinado ponto do tempo.

> <img src="/ico/blob_confused.png" width="30"/>
Um pouquinho complicado, né? Mas você vai pegar rapidinho

O importante para agora é entendermos que o versionamento de um repositório é organizado através de uma **árvore de commits**, isso é, ramificações (**branches**) de uma **história**, de modo que tudo começa no *commit raiz* e a partir dele vai evoluindo. Cada **branch** representa uma linha do tempo que possui um ancestral comum com os demais branches do repositório, seja esse o **commit** raiz ou um posterior.

Desse modo é usual que um dos branches seja tratado como o principal daquele repositório, corriqueiramente nomeado `main` ou `master`. Os demais branches terão caráter temporário servindo para organizar as evoluções do repositório e sendo, por fim, integrados novamente ao branch principal.


## Comandos
Para auxiliar a gestão do versionamento de um repositório o git traz uma série de comandos, cada um voltado para uma situação específica. Vamos passar por vários deles endendendo em que momento utilizá-los.

> Nessa sessão vamos demonstrar os commandos da [CLI][cli] do git como executados através do [Shell][shell]. Campos que devem ser substituídos conforme o contexto serão apresentados entre cerquilhas (< >)

[shell]: /4.extra/shell.md
[cli]: /4.extra.cli.md


### Descobrindo e configurando
O git é uma ferramenta muito poderosa e bem madura, assim traz consigo uma série de informações e a possibilidade de configurar sua utilização. Vejamos como:

#### `git help`
Esse pode ser o comando mais importante na evolução do seu uso do git. Se usado sem argumentos te apresentará uma lista com os principais comandos (que também veremos aqui) junto com uma breve descrição de para que servem.

Caso queira saber mais detalhes sobre um determinado comando, `git help <command>` te mostrará o manual do git com todas as informações do comando escolhido. Alternativamente `git <command> --help` também te levará ao mesmo conteúdo.

Por fim `git <command> -h` mostrará uma versão resumida do *help*, descrevendo o uso do comando escolhido e suas principais *flags*

#### `git config`
O git nos permite armazenar configurações em grupos através do commando `git config <group>.<key> <value>`. Como resultado estaremos editando um arquivo nomeado `.gitconfig` que posteriormente será consultado pelos demais comandos durante sua utilização.

Por exemplo, ao configurarmos `user.name` e `user.email`, satisfazemos a necessidade de nos identificarmos repetidamente durante a execução dos comandos que demandam registro de autoria.

Por padrão essas configurações são armazenadas apenas no repositório atual, porém a *flag* `--global` as registrará no seu usuário do sistema operacional, servindo para os inúmeros repositórios que você vier a manejar naquela maquina


### Iniciando um repositório
Para realizar o controle de versões, o git armazena uma série de estruturas no diretório `.git`. A pasta que contém esse diretório é o que chamamos de repositório e todos os arquivo internos a ela serão monitorados para o controle de versões.

#### .gitignore
O `.gitignore` é um arquivo de configuração em que listamos uma série de [expressões regulares][regex] quais o git usa para selecionar arquivos que serão ignorados durante o monitoramento do repositório

[regex]: https://pt.wikipedia.org/wiki/Express%C3%A3o_regular

#### `git init`
Para iniciar um novo repositório a partir do diretório atual executamos o comando `git init`. Isso resultará na criação do subdiretório `.git`, qual traz consigo um branch de nome `master` configurado.

Opcionalmente podemos referenciar um diretório como argumento do comando (`git init <directory>`) para que seje esse a raiz do novo repositório.

#### `git clone`
Caso o repositório já exista remotamente, o commando `git clone <repo url>` copiará seus os arquivos para a maquina local. Uma nova pasta com o nome do repositório será criada sob o diretório corrente e você já estará apto a aplicar alterações no projeto.

A url de um repositório tem a forma `git@<HOST>:<USER>/<REPO>.git` onde:

| Campo | Significado | Exemplo |
| --- | --- | --- |
| HOST | Serviço de gerenciamento de repositórios git | gitlab.nelogica.com.br |
| USER | Usuário ou organização que mantém o projeto | afraga |
| REPO | Nome do repositório | welcome |


### Consultando e incluindo alterações
Como já sabemos o próposito de uso do git é gerenciar alterações sob o *código-fonte* de um projeto. Nesse ponto vale descrever que realizar alterações num arquivo do repositório não evolui sua **árvore de commits** automaticamente.

O que acontece é que, no git, uma dada modificação pode estar em três diferentes situações: *untracked*, *unstaged* ou *staged*. E esse status influencia o efeito que determinados comandos terão sob ela.

Por exemplo, novos arquivos começam *untracked*. Isso significa que esse existe localmente, porém ainda não está sendo monitorado pelo git. Não há nenhuma versão dele registrada na **história** do **branch** atual.

Arquivos que já estão sendo monitorados mostrarão suas modificações na área de *unstaged*. Isso significa que o git está ciente que o arquivo sofreu alterações e essas estão disponíveis para serem adicionadas.

#### `git add`
O comando `git add <file>` adiciona as alterações de um arquivo *untracked* ou *unstaged* à área de *staging*. Ele registra que se quer incluir as atualizações do arquivo particular em seu próximo **commit**. No entanto, esse comando ainda não afeta realmente a **história** do repositório de nenhuma forma — as alterações não são realmente gravadas até você executar `git commit` (que veremos a seguir).

> É possível selecionar múltiplos arquivos para serem adicionados de uma só vez. Seja listando-os um por um como argumentos do commando, `git add file.txt file.md another-file`, ou ainda selecionando todos de um diretório, `git add dir/*`. Além disso também é possivel usar a *flag* `--all` (`-A`) para selecionar todos os arquivos alterados.

#### `git status`
Para inspecionar a situação atual das modificações do repositório, execute `git status`. Isso listará os arquivos alterados, identificando qual o estágio de suas modificações (*untracked*, *unstaged* ou *staged*), e descrevendo com quais comandos manipulá-las.

A *flag* `--short` (`-s`) apresenta as mesmas informações, porém de maneira sussinta e colorida. Cada arquivo é apresentado numa linha, cuja cor identifica se suas modificações estão *unstaged* (vermelho) ou *staged* (verde). O tipo de modificação também é apresentado, neste caso por um símbolo no início da linha, conforme a seguinte legenda:

| Símbolo | Significado |
| --- | --- |
| ?? | *untracked* |
| M | modified |
| A | added |
| D | deleted |
| R | renamed |
| C | copied |

> <img src="/ico/blob_detective.png" width="30"/>
Para mais detalhes sobre o *short format* consulte a documentação oficial [aqui][short-format]

[short-format]: https://git-scm.com/docs/git-status#_short_format

#### `git diff`
Outra maneira de inspecionar as modificações do repositório é checar em detalhes as alterações no corpo de um determinado arquivo. Isso pode ser feito executando `git diff <file>`. Quando executado exibirá cada um dos trechos modificados, identificando as inserções em verde, as deleções em vermelho e o que não fora alterado em branco.

Observe que, mesmo que sejam apresentadas nas mesmas cores, há uma distinção do significado delas em relação ao `git status`.

> Semelhantemente ao `git add`, mútiplos arquivos, um diretório ou mesmo todos os arquivos podem ser selecionados pelo comando. Para o último caso, basta que nenhum argumento seja inserido, isto é apenas `git diff`

Com a *flag* `--stat`, o output do comando se resume a apresentar o número de modificações identificadas em cada um dos arquivos selecionados, consolidando o total de arquivos e de modificações ao final.

Também é possível visualizar essas modificações com o auxílio de uma interface gráfica. Dentre alternativas há o [meld][meld] que pode ser acessado através do comando `git difftool --tool meld`.

[meld]: https://meldmerge.org/

#### `git commit`
Quando finalmente queremos salvar as modificações da área de *staging*, evoluindo a **história** do repositório, executamos `git commit`. Você será redirecionado para um espaço onde poderá escrever a mensagem que o identificará. Ao concluir, salve-a e está tudo pronto!

> Opcionalmente você pode realizar esse processo em uma única etapa, definindo a mensagem através da *flag* `--message` (`-m`) da seguinte forma: `git commit -m <message>`

A execução desse commando resultará em um novo ponto na linha do tempo de modificações do projeto, integrando definitivamente suas evoluções ao repositório.

#### `git log`
Para consultar a sequência cronológica dos marcos (isto é, **commits**) da **historia** do **branch** atual, executa-se `git log`. Com isso, será exibido para cada **commit**, seu SHA-1, autor, data e mensagem, identificando também onde encontram-se os **branches** locais e remotos.

Por padrão toda a **história** é exibida, asssim, quando se quer limitar a exibição em `N` **commits** pode-se usar a *flag* `-<N>`. Ou ainda, para exibí-los de maneira contensada, `--pretty=oneline`, pode ser sua alternativa.


### Sincronizando evoluções
O git foi projetado para dar a cada desenvolvedor um ambiente de desenvolvimento totalmente isolado. E sincronizar essas evoluções realizadas simultânea e distribuidamente é o grande ponto aqui. Vamos ver como fazer isso!

É comum termos o código-fonte de um projeto hospedado em um serviço remoto de repositórios git. Nós aqui usamos o [Gitlab][gitlab]! Assim, todos os desenvolvedores podem acessar e contribuir com o projeto de onde quer que estejam.

Dessa maneira, para vários comandos do git precisaremos referenciar o **branch** de um repositório remoto. E é para que não tenhamos que relembrar e reescrever longas URLs repetidamente que temos um facilitador pra isso:

[gitlab]: /1.intro/gitlab.md

#### `git remote`
O comando `git remote` permite criar, ver e excluir conexões com repositórios, associando um *nome* para cada uma delas. Quando você clona um repositório com `git clone`, ele automaticamente cria uma conexão remota chamada `origin`, que aponta de volta para o repositório clonado.

Para listar as conexões do repositório corrente basta executar `git remote`. Qual, quando usando junto à *flag* `--verbose` (`-v`) apresenta, além do nome, a URL associada a cada conexão.

Você pode adicionar novas conexões com `git remote add <name> <url>`. Pode renomeá-las (`git rename <actual-name> <new-name>`). Ou excluí-las (`git remote remove <name>`). E ainda mais... confira todas as opções através do `git remote -h`!

#### `git fetch`
Executanto `git fetch <remote>`, trazemos para o repositório local as informações mais atualizadas do estado dos repositórios remotos, mas sem modififcar a **história** do seu branch atual.

> Mais uma vez podemos incluir vários repositórios como argumento do comando e assim trazer informações sobre todos deles. Opcionalmente, se não incluirmos argumento algum, faremos `fetch` em todos nossos repositórios remotos

#### `git pull`
Para, então, baixarmos novas evoluções do projeto contidas em um repositório remoto, dessa vez sim integrando-as à **história** do nosso branch atual, executamos `git pull <remote> <branch>`. Esse é na verdade a combinação do `git fetch` com um dos comandos que promovem a integração dos códigos-fonte.

Por padrão `git merge` é usado, porém a *flag* `--rebase` (`-r`) permite selecionarmos outra estratégia de integração, o `git rebase`. Veremos seus funcionamentos em mais detalhes mais a frente.

#### `git push`
De maneira análoga ao pull, `git push <remote> <branch>` é usado para enviar conteúdo do branch local para um remoto. É o modo com que os **commits** que carregam as evoluções que realizamos são novamente sincronizados com um repositório remoto.

> O Git evita que você sobrescreva o histórico do repositório remoto, recusando seu push quando esse resultaria em um merge. Isso é, se o histórico remoto for diferente do seu, você precisa fazer o pull do branch remoto e o merge com seu branch local para, em seguida, tentar fazer o push de novo

Para casos em que queremos forçar a sobreescrita do branch remoto, a *flag* `--force` (`-f`) pode ser usada. Mas use-a com cautela e sabedoria! \
Outra *flag* útil é a `--set-upstream` (`-u`) que vai associar seu branch local com o remoto, permitindo a omissão dos argumentos (`<remote> <branch>`) nas próximas execuções do comando.


### Manipulando o contexto de trabalho
Nesse ponto vale evidenciar que o que o git chama de **branch** é apenas uma referência para um determionado **commit**. Ou seja, ao criar uma ramificação na **história** do repositório, tudo o que fazemos é criar um novo *ponteiro nomeado*, sem realizar modificações de nenhuma outra maneira.

Também é importante sabermos que associado a um repositório local, o git chama de `HEAD` uma referência que aponta para o **branch** no qual estamos trabalhando. o `HEAD` serve para a identificação do **commit** base das operações e modificações que você executar.

Além disso quando falarmos sobre sua **working tree**, estamos tratando do contexto atual de execução do git num dado repositório, abrangendo especialmente para qual branch aponta o `HEAD`, a versão commitada dos arquivos naquele branch e todas as modificações em aberto (*untracked*, *unstaged* e *staged*).

#### `git branch`
Para listar os branches locais, executamos `git branch`. E se adicionarmos a *flag* `--all` (`-a`) exibiremos também os branches dos repositórios remotos que registramos com `git remote`.

`git branch <branch>` cria um novo branch. Porém, se adicionarmos a *flag* `--delete` (`-d`) iremos, na verdade, deletar o branch escolhido.

#### `git checkout`
Já quando se trata de manipular sua **working tree** utilizamos o comando `checkout`. Ele é responsavel por atualizar os arquivos do repositório para uma determinada versão, isso é, regressando-os para seu formato em um especifico **commit**. Assim, `git checkout <revision>` pode ser usado com dois alvos distintos.

Se usado com um `<branch>` como argumento, vai alterar sua **working tree**, apontando o `HEAD` para o branch indicado e levando suas modificações junto.

Porém se usado selecionando um arquivo (um conjunto de arquivos ou mesmo um diretório), o resultado será a restauração desse(s) para seu estado commitado, qual aponta o `HEAD`, isso é, descartando suas modificações abertas (*untracked*, *unstaged* e *staged*).

#### `git stash`
Em ocasiões que precisamos reservar o trabalho em andamento (as modificações em aberto), ao mesmo tempo garantindo que não as perderemos e também limpando a **working tree**, podemos usar o `git stash`.

Quando executado sem argumentos, reservará todas as modificações *unstaged* e *staged* em um lugar do qual poderão ser restauradas depois. E com a *flag* `--include-untracked` (`-u`), reservamos também as modificações *untracked*.

Executanto, então, `git stash pop` restauramos as modificações anteriormente reservadas retomando-as para sua **working tree**.


## Integrando versões
Para realizar a integração entre duas versões distintas de um repositório, o git inspeciona a **história** de cada um dos **branches** identificando o **commit** mais recente que está presente em ambas, isso é seu *ancestral comum*. Esse servirá de base para distinguir a diferença entre as modificações de cada ramificação durante seu processo de unificação.

#### `git merge`
A estratégia de merge funciona elencando todas as modificações do branch selecionado que não estão presentes na história do branch atual e as combinando em um único novo commit.

Assim, ao executarmos `git merge <branch>` evoluímos a **história** do **branch** atual em um **commit**, qual conterá todas as modificações do branch selecionado que são posteriores ao *ancestral commum*.

#### `git rebase`
Outra estratégia de integração é o rebase, qual evoluirá a **história** do **branch** atual em múltiplos novos **commits**, um para cada commit que o branch selecionado tiver a partir do *ancestral comum*.

#### `git cherry-pick`
Quando é o caso de querermos trazer um **patch** (modificações de um determinado **commit**) para nosso **branch** atual, executamos `git cherry-pick <patch>`. Isso resultará na integração de tais modificações através de um novo **commit** no final da **história** do nosso **branch**.

#### Resolvendo conflitos
Algumas vezes, durante o processo de merge, pode ocorrer de um mesmo arquivo ter sido alterado no mesmo ponto em ambas ramificações e isso vir a causar um conflito.

Conflitos ocorrem quando o git não consegue determinar sozinho qual das modificações selecionar para compor o estado final resultante do processo de integração

#### `gitk`
O gitk é uma alternativa gráfica para manipulação do git e pode ser útil durante a resolução de conflitos, pois aprensetará ambas versões frente a frente, permitindo a seleção manual do estado final das modificações que irão compor o merge.

Para utilizá-lo execute `gitk`. Opcionalmente a *flag* `--all` apresentará todos branches, o que é particularmente útil para depuração durante rebases.


## Trabalhando com Submodulos
O git traz também alternativas para manipular múltiplos repositórios como se fossem um só, ainda assim manejando o versionamento de suas partes de maneira independente.

Para casos como esse observaremos na raiz do repositório composto um arquivo de configuração chamado `.gitmodules`, qual armazenará informações sobre essa composição. E será preciso inicializarmos os submódulos após o clone do repositório raiz.

#### `git submodule`
Isso pode ser atingido executando `git submodule init`. Ou opcionalmente utilizando a *flag* `--recurse-submodules` durante a própria execução do `git clone`.

Também é possível manipular os submódulos de um repositório. Para adicionar uma nova dependência executamos `git submodule add <repo url>`. Ou ainda para atualizar a versão dos submodulos existentes executamos `git submodule update`

---
> Esse conteúdo é uma adaptação dos excelentes [tutoriais sobre git da Atlassian][git-atlassian]. Caso te interesse saber mais, lá é possivel encontrar muito mais informações e exemplos sobre o uso da ferramenta
>
> Caso queira algo mais prático, recomendamos o [Learn git branching][git-branching] e o [Visualizing git][git-visualizing], que trarão um acompanhamento visual da arvore de commits durante sua manipulação

[git-atlassian]: https://www.atlassian.com/br/git/tutorials/what-is-version-control
[git-branching]: https://learngitbranching.js.org
[git-visualizing]: https://git-school.github.io/visualizing-git/


## Questões

Após aprender a criar o setup.md na secção referente ao GitLab, crie de forma análoga no diretório answers um arquivo git.md e responda as perguntas abaixo:

- O que é fast-forward?
- Qual a diferença de merge e rebase?
- O que é um remoto no git?
- Como se usa o git alias?
- O que é o arquivo `.gitconfig`?
- Qual a diferença entre realizar um rebase e criar um branch novo a partir do branch ao qual se vai fazer rebase e fazer cherry-pick de cada commit do branch novo não presente no branch antigo?
- O que é um merge request?
