# Sumário

* [Bem vindo!](README.md)

## Introdução
* [Fluxo de integração de código](/1.intro/workflow.md)
* [Git](/1.intro/git.md)
* [Gitlab](/1.intro/gitlab.md)
* [Domínio de Aplicação](/1.intro/dominio.md)
* [Jira Software](/1.intro/jira.md)
* [Informações Adicionais](/1.intro/extra.md)

## Linguagens de programação
* [Delphi](/2.code/delphi/index.md)
    * [Pascal Moderno](/2.code/delphi/object-pascal.md)
    * [Padrão de codificação](/2.code/delphi/padrao-codigo.md)
    * [Problemas Comuns](/2.code/delphi/faq.md)
    * [Criação de projeto](/2.code/delphi/novo-projeto.md)
    * [Edição de um projeto existente](/2.code/delphi/projeto-existente.md)
    * [Criação de testes unitários](/2.code/delphi/teste.md)
    * [Questões](/answers/delphi.md)

## Times
* [Electronic Trading Systems](/3.team/ets.md)

* [Market Data](/3.team/market-data/index.md)
    * [Configurar FTP](/3.team/market-data/ftp.md)
    * [Instalar WatchDogInstrumentation](/3.team/market-data/watchdog.md)

* [Order Management System (OMS)](/3.team/oms.md)

* [Quality Assurance (QA)](/3.team/qa/index.md)
    * [A filosofia do bug](/3.team/qa/filosofia-bug.md)
    * [Métricas de código](/3.team/qa/metricas.md)
    * [Testes](/3.team/qa/testes.md)
    * [Integração Contínua, Entrega e Deploy contínuos](/3.team/qa/ci-cd.md)

## Outros
* [CLI](/4.extra/cli.md)
* [Shell](/4.extra/shell.md)
* [SQL](/4.extra/sql.md)
* [SSH](/4.extra/ssh.md)
* [VPN](/4.extra/vpn.md)
